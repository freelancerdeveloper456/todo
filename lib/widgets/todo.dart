import 'package:flutter/material.dart';

class Todo extends StatelessWidget {
  final String title;
  final String description;
  final String time;
  const Todo({super.key, required this.title, required this.description,required this.time});
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(color: Colors.orange,borderRadius:BorderRadius.circular(10)),
            child:Padding(
              padding:const EdgeInsets.all(7.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
           
                children: [Text(title,style:const TextStyle(fontWeight:FontWeight.bold,fontSize:20)), Text(description), Text(time)],
              
            ))));
  }
}
