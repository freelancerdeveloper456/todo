import "package:flutter/material.dart";
import "package:todo/widgets/todo.dart";

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(centerTitle: true, title: Text('Todo App')),
        body: Column(
          children: [
            Todo(title:'Office work',description: 'Remind me for office work.',time:'09:00'),
            Todo(title:'abc',description: 'fgdfgfdgfdg',time:'03:00'),
            Todo(title:'xyz',description: 'fgdfgj.',time:'06:00'),
          ],
        )
        
        
        
        
        
        );
  }
}
